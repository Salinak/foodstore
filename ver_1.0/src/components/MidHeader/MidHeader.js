import React, { useState } from 'react';
import { Input, NavbarBrand } from 'reactstrap';
import search from '../../icons/search.svg';
import './MidHeader.scss';

const MidHeader = (props) => {
    return (
        <div className="mid-header">
            <div className="container">
                <ul className="d-lg-flex justify-content-between align-items-center mb-0">
                    <li className="d-none d-lg-block">
                        <NavbarBrand href="/">FOOD STORE</NavbarBrand>
                    </li>
                    <li>
                        <form action="#">
                            <div class="input-group">
                                <Input type="search" name="search" id="search" placeholder="SEARCH" />
                                <div class="input-group-prepend">
                                    <button className="btn">
                                        <img src={search} width="15" alt="search" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default MidHeader;
