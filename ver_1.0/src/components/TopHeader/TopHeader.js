import React, { useState } from 'react';
import fb from '../../icons/facebook.svg';
import linkedin from '../../icons/linkedin.svg';
import insta from '../../icons/instagram.svg';
import './TopHeader.scss';

const TopHeader = (props) => {
    return (
        <div className="top-header">
            <div className="container">
                <ul className="d-flex justify-content-between">
                    <ul className="d-flex">
                        <li>
                            <a href="#">
                                <img src={fb} width="20" alt="search" />
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src={insta} width="20" alt="search" />
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src={linkedin} width="20" alt="search" />
                            </a>
                        </li>
                    </ul>
                    <ul className="d-flex account">
                        <li>
                            <a href="#">MY ACCOUNT</a>
                        </li>
                        <li>
                            HELLO, GUEST
                        </li>
                    </ul>
                </ul>
            </div>
        </div>
    );
}

export default TopHeader;