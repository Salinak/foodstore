import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText
} from 'reactstrap';
import './BottomHeader.scss';

const BottomHeader = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <>
            <Navbar expand="lg">
                <div className="container">
                    <NavbarBrand href="/" className="d-lg-none">FOOD STORE</NavbarBrand>
                    <button aria-label="Toggle navigation" onClick={toggle} type="button" className={`nav-icon navbar-toggler ${isOpen ? "open" : ""}`}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <Collapse isOpen={isOpen} navbar className="justify-content-lg-center">
                        <Nav navbar>
                            <NavItem>
                                <NavLink href="#">Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">About</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Products</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Contact</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Store Policies</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Special Offers</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">FAQ</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </div>
            </Navbar>
        </>
    );
}

export default BottomHeader;
