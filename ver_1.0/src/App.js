import './App.scss';
import TopHeader from './components/TopHeader/TopHeader';
import MidHeader from './components/MidHeader/MidHeader';
import BottomHeader from './components/BottomHeader/BottomHeader';
import MainSection from './components/MainSection/MainSection';

function App() {
    return (
        <>
            <header className="shadow-sm">
                <TopHeader />
                <MidHeader />
                <BottomHeader />
            </header>
        </>
    );
}

export default App;
